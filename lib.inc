%define EXIT_SYSCALL 60
%define SHIFT 10
%define TO_ASCII 48
section .text

; Принимает код возврата и завершает текущий процесс 
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .next_symb:
        mov rdx, [rdi]
        cmp dl, 0
        jz .finish
        inc rax
        inc rdi
        jmp .next_symb
    .finish:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    .print_next_symb:
        mov rdx, [rdi]
        cmp dl, 0   
        jz .finish_print_string ;if symb. is 0 -> return
        mov rax, 1              ;write
        mov rsi, rdi            ;symb. adress
        push rdi
        push rdx
        mov rdi,1               ;stdout
        mov rdx, 1              ;1 symb
        syscall
        pop rdx
        pop rdi
        inc rdi
        jmp .print_next_symb
    .finish_print_string:
        ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax,1 
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    dec rsp
    mov byte [rsp], 0              ;the end of string
    mov rax, rdi
    mov r11, 1                     ;this reg is for returning rsp
    mov r10, SHIFT
    .next_div:
        xor rdx, rdx    
        div r10
        add dx, TO_ASCII                 ;make ASCII number
        dec rsp
        mov [rsp], dl
        inc r11
        cmp rax, 0
        jnz .next_div
    .print_number:
        mov rdi, rsp
        push r11
        call print_string
        pop r11
        add rsp, r11
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .positive
    .negative:
        push rdi
        mov rdi, 45         ;"-" in ASCII
        call print_char
        pop rdi
        neg rdi             ;now number is positive
    .positive:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .compare_next_symb:
        mov r10b, [rdi+rax]
        mov r11b, [rsi+rax]
        cmp r10b, r11b
        jnz .finish_with_code_0
        cmp r10b, 0
        jz .finish_with_code_1
        inc rax
        jmp .compare_next_symb
    .finish_with_code_1:
        mov rax, 1
        ret
    .finish_with_code_0:
        xor rax, rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdx
    push rdi
    push rsi
    dec rsp
    mov rsi, rsp
    xor rax, rax    ;rax = 0
    xor rdi, rdi    ;rdi = 0
    mov rdx, 1
    syscall         ;system call: read(0,rsp,1)
    test rax, rax
    jz .finish
    mov al, [rsp]
    .finish:
        inc rsp
        pop rsi
        pop rdi
        pop rdx
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r10, r10            ;incrementor for loops (rdi) a.k.a word length
    mov r11, rsi            ;buffer length
    cmp r11, 0
    jz .null_buffer_exception
    dec r11                 ;I need 1 symb as a null-terminator
    .start_loop:
        push r11
        call read_char
        pop r11
        cmp rax, 0x20
        jz .start_loop
        cmp rax, 0x9
        jz .start_loop
        cmp rax, 0xA
        jz .start_loop 
        cmp r11, 0
        jz .word_too_long
        test rax, rax
        jz .finish
        mov [rdi+r10], al
        inc r10
        dec r11
    .main_loop:
        push r11
        call read_char
        pop r11
        cmp rax, 0x20
        jz .finish
        cmp rax, 0x9
        jz .finish
        cmp rax, 0xA
        jz .finish
        cmp rax, 0
        jz .finish
        cmp r11, 0
        jz .word_too_long
        mov [rdi+r10], al
        inc r10
        dec r11
        jmp .main_loop
    .finish:
        mov byte [rdi+r10], 0
        mov rdx, r10                ;string length
        mov rax, rdi
        ret
    .word_too_long:
        xor rax, rax
        mov byte [rdi+r10], 0     ;null-term.
        ret
    .null_buffer_exception:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx 
    xor rsi, rsi
    mov r10, 10     ;just constant for computation
    .next_symb:
        xor r11, r11
        mov r11b, [rdi+rsi]
        cmp r11b, 48    ;[0-9] ~ 48-57 in ASCII
        jl .finish
        cmp r11b, 57
        jg .finish
        mul r10
        add rax, r11
        sub rax, 48
        inc rsi
        jmp .next_symb
    .finish:
        mov rdx, rsi
        ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], 45      ;minus in ASCII
    jnz .parse
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
    .parse:
        call parse_uint
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r11, r11
    push rdi
    push rdx
    call string_length
    pop rdx
    pop rdi
    cmp rax, rdx
    jg .finish_with_code_0
    .loop:
        cmp rdx, 0
        jz .finish
        mov r10b, [rdi+r11]
        mov [rsi+r11], r10b
        inc r11
        dec rdx
        jmp .loop
    .finish:
        ret
    .finish_with_code_0:
        xor rax, rax
        ret
